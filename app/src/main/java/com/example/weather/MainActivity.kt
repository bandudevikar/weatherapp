package com.example.weather
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.weather.database.entity.Bookmark
import com.example.weather.databinding.MainActivityBinding
import com.example.weather.ui.help.UtilsFragment
import com.example.weather.ui.mark.MarkFragment
import com.example.weather.ui.bookmarkmap.MapFragment
import com.example.weather.ui.city.CityLocationsFragment

/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
class MainActivity : AppCompatActivity(), MapFragment.MapListener,
    MarkFragment.BookMarkListener {
    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        initializeView()
    }

    private fun initializeView() {
        addFragment(MarkFragment.newInstance(), MarkFragment.BOOKMARK_FRAGMENT)
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        if (fragment is MarkFragment) {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragmentContainer, fragment, tag).commit()

        } else {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragmentContainer, fragment, tag).addToBackStack(null).commit()

        }
    }

    override fun onFabClick() {
        addFragment(MapFragment.newInstance(), MapFragment.MAP_FRAGMENT)
    }

    override fun onLocationBookMarked(bookmark: Bookmark) {
        val fragment: MarkFragment =
            supportFragmentManager.findFragmentByTag(MarkFragment.BOOKMARK_FRAGMENT) as MarkFragment
        fragment.let {
            fragment.addBookMarkedLocation(bookmark)
        }

    }

    override fun onCitySelected(bookMark: Bookmark) {
        val cityFragment = CityLocationsFragment.newInstance()
        cityFragment.arguments = Bundle().apply {
            putParcelable(CityLocationsFragment.CITY, bookMark)
        }
        addFragment(cityFragment, CityLocationsFragment.CITY_FRAGMENT)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.help) {
            addFragment(UtilsFragment.newInstance(), UtilsFragment.HELP_FRAGMENT)
        }

        return super.onOptionsItemSelected(item)
    }

}