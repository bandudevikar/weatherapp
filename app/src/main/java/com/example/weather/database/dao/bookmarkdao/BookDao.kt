package com.example.weather.database.dao.bookmarkdao
import androidx.room.*
import com.example.weather.database.entity.Bookmark
/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
@Dao
interface BookDao {
    @Insert
    suspend fun insert(bookmark : Bookmark) : Long

    @Query("SELECT * FROM bookmark ORDER BY id DESC")
    suspend fun getBookmarks() : List<Bookmark>

    @Delete
    suspend fun delete(bookmark : Bookmark)

}