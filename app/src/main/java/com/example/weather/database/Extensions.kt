package com.example.weather.database
import androidx.core.view.isVisible
import androidx.core.widget.ContentLoadingProgressBar
/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
fun ContentLoadingProgressBar.showProgress(){
    isVisible = true
    animate()
}

fun ContentLoadingProgressBar.hideProgress(){
    isVisible = false
}