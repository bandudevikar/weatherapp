package com.example.weather.network
import com.example.weather.BASE_URL
import com.example.weather.network.response.WeatherResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
/**
 * Created by       : Bandu
 * Date             : 17/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
interface ApiService {
    @GET("weather")
    suspend fun getWeatherData(@Query("units") units: String,
                               @Query("lat") lat: String,
                               @Query("lon") lon: String,
                               @Query("appid") apiKey: String):Response<WeatherResponse>



    /**
     * Created by       : Bandu
     * Date             :
     * Purpose/Usage    : Demo purpose to explain use
     *			    Change – Add in add()
     *
     *                    2 - ABC
     *                    Change – Modify Substract()
     *
     * Input Values	  : Integer, Object from XYZ() or XYZ Class
     * Expected Output  : Returning Filtered Object with Additional Values of User
     * 			    Profile
     *
     * Additional Comments -
     */

    companion object {
        operator fun invoke(): ApiService{
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .build()
                .create(ApiService::class.java)
        }
    }
}