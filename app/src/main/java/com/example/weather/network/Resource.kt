package com.example.weather.network
/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
data class Resource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.Success, data, null)
        }
        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.Error, data, msg)
        }
        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.Loading, data, null)
        }

    }
}