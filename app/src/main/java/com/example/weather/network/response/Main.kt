package com.example.weather.network.response

/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
data class Main(
    val temp: Double,
    val feels_like: Double,
    val temp_min: Double,
    val temp_max: Double,
    val pressure: Int,
    val humidity: Int,
    val sea_level: Int,
    val grnd_level: Int
)