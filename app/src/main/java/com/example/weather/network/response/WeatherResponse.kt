package com.example.weather.network.response
/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
data class WeatherResponse(
    val coord: Coord,
    val weather: List<Weather>,
    val base: String,
    val main: Main,
    val visibility: Int,
    val wind: Wind,
    val rain: Rain,
    val clouds: Clouds,
    val dt: Int,
    val sys: Sys,
    val timezone: Int,
    val id: Int,
    val name: String,
    val cod: Int
)
