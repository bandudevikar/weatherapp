package com.example.weather.network.response
import com.squareup.moshi.Json
/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
data class Rain(@Json(name = "3h") val threeHourlyVolume: Double)