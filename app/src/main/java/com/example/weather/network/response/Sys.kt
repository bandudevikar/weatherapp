package com.example.weather.network.response
/**
 * Created by       : Bandu Devikar
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
data class Sys(
    val country: String,
    val sunrise: Int,
    val sunset: Int
)