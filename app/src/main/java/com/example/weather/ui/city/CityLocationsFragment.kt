package com.example.weather.ui.city
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.weather.database.entity.Bookmark
import com.example.weather.databinding.CityFragmentBinding
import com.example.weather.network.Status

/**
 * Created by       : Bandu Devikar
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
class CityLocationsFragment : Fragment() {
    lateinit var binding: CityFragmentBinding
    private lateinit var locationsViewModel: CityLocationsViewModel
    private var bookMark: Bookmark? = null
    companion object {
        fun newInstance() = CityLocationsFragment()
        const val CITY = "city"
        const val CITY_FRAGMENT = "cityFragment"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bookMark = arguments?.getParcelable(CITY)
        binding = CityFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        locationsViewModel = ViewModelProvider(this).get(CityLocationsViewModel::class.java)
        bookMark?.let { bookMark ->
            activity?.let {
                locationsViewModel.getCityWeatherData(bookMark).observe(it) { resource ->

                    when (resource.status) {

                        Status.Loading -> {
                            binding.progressHorizontal.isVisible = true
                            binding.progressHorizontal.animate()
                        }

                        Status.Success -> {
                            resource.data?.let { weather ->

                                if (weather.isSuccessful) {

                                    binding.progressHorizontal.isVisible = false
                                    binding.locationCard.isVisible = true
                                    binding.weathercard.isVisible = true

                                    weather.body()?.let { weatherResponse ->

                                        with(binding) {

                                            tvLocation.text = weatherResponse.name

                                            tvMaxTemp.text =
                                                weatherResponse.main.temp_max.toString()

                                            tvMinTemp.text =
                                                weatherResponse.main.temp_min.toString()

                                            tvhHumidity.text =
                                                weatherResponse.main.humidity.toString()

                                            tvWind.text = weatherResponse.wind.speed.toString()

                                            tvPressure.text =
                                                weatherResponse.main.pressure.toString()

                                            tvSea.text = weatherResponse.main.sea_level.toString()
                                        }

                                    }

                                }

                            }
                        }

                        Status.Error -> {
                            Toast.makeText(activity, resource.message, Toast.LENGTH_LONG).show()
                        }

                    }

                }
            }
        }
    }

    override fun onResume() {
        bookMark?.let {
            activity?.title = it.city
        }
        super.onResume()
    }

}