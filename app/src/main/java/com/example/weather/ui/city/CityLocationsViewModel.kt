package com.example.weather.ui.city

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.weather.OPEN_WEATHER_API_KEY
import com.example.weather.database.entity.Bookmark
import com.example.weather.network.Resource
import java.lang.Exception

/**
 * Created by       : Bandu Devikar
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */

class CityLocationsViewModel : ViewModel() {

    fun getCityWeatherData(bookmark: Bookmark) = liveData {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = CityLocationRepository().getWeatherData(units = "metric",lat =bookmark.lat, lon = bookmark.lon, apiKey = OPEN_WEATHER_API_KEY )))
        }catch (exception: Exception){
            emit(Resource.error(data = null, msg = exception.message?:"Error Occurred"))
        }
    }

}