package com.example.weather.ui.city

import com.example.weather.network.ApiService
import com.example.weather.network.response.WeatherResponse
import retrofit2.Response
/**
 * Created by       : Bandu Devikar
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
class CityLocationRepository {

    suspend fun getWeatherData(
        units: String,
        lat: String,
        lon: String,
        apiKey: String
    ): Response<WeatherResponse> {

        return ApiService().getWeatherData(units,lat, lon, apiKey)
    }

}