package com.example.weather.ui.mark
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weather.R
import com.example.weather.database.entity.Bookmark
import com.example.weather.database.hideProgress
import com.example.weather.database.showProgress
import com.example.weather.databinding.BookMarkFragmentBinding
import com.example.weather.network.Status
/**
 * Created by       : Bandu
 * Date             : 18/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
class MarkFragment : Fragment() {
    lateinit var binding: BookMarkFragmentBinding
    private lateinit var listener: BookMarkListener
    private lateinit var markAdapter: MarkAdapter
    private lateinit var viewModel: MarkViewModel
    companion object {
        fun newInstance() = MarkFragment()
        const val BOOKMARK_FRAGMENT = "bookMarkFragment"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = BookMarkFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        markAdapter = MarkAdapter(ArrayList(),
            onItemClick = { bookMark ->
                listener.onCitySelected(bookMark = bookMark)
            },

            onItemDeleteClick = { bookMark ->
                activity?.let {
                    showDialog(context = it,bookMark = bookMark)
                }
            })
        binding.rvBookmarks.adapter = markAdapter
        binding.rvBookmarks.layoutManager = LinearLayoutManager(activity)

        binding.fab.setOnClickListener {
            if (::listener.isInitialized) {
                listener.onFabClick()
            }
        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MarkViewModel::class.java)

        activity?.let {

            viewModel.bookMarkLiveData.observe(it) { resource ->
                when (resource.status) {

                    Status.Loading -> {
                            binding.progressHorizontal.showProgress()
                    }

                    Status.Success -> {
                        resource.data?.let {bookMarkList->
                            if (bookMarkList.isNotEmpty()){
                                binding.tvEmptyMsg.isVisible = false
                                binding.rvBookmarks.isVisible = true
                                setBookMarkData(resource.data)
                            }else{
                                binding.tvEmptyMsg.isVisible = true
                            }
                        }
                        binding.progressHorizontal.hideProgress()
                    }

                    Status.Error -> {
                        binding.tvEmptyMsg.isVisible = false
                        binding.progressHorizontal.hideProgress()
                        Toast.makeText(activity, resource.message, Toast.LENGTH_LONG).show()
                    }

                }
            }

            viewModel.deleteLiveData.observe(it) { resource ->
                when (resource.status) {

                    Status.Loading -> {

                    }

                    Status.Success -> {
                        resource.data?.let {bookMark->
                            markAdapter.removeItem(bookMark)
                        }
                    }

                    Status.Error -> {
                        Toast.makeText(activity, resource.message, Toast.LENGTH_LONG).show()
                    }

                }
            }
            viewModel.getBookMarked()
        }
    }

    override fun onResume() {
        activity?.title = getString(R.string.app_name)
        super.onResume()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser){
            activity?.title = getString(R.string.app_name)
        }
        super.setUserVisibleHint(isVisibleToUser)
    }

    private fun setBookMarkData(bookMarkList: List<Bookmark>) {
        if (::markAdapter.isInitialized) {
            markAdapter.addData(bookMarkList)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as BookMarkListener
        } catch (exception: Exception) {

        }
    }

    fun addBookMarkedLocation(bookMark: Bookmark){
        markAdapter.addData(listOf(bookMark))
    }

    private fun showDialog(context: Context, bookMark: Bookmark) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage("Do you want to delete bookmarked location ${bookMark.city}?")

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            viewModel.deleteBookMark(bookMark = bookMark)
            dialog.dismiss()
        }

        builder.setNegativeButton(android.R.string.no) { dialog, which ->
            dialog.dismiss()
        }

        builder.show()
    }

    interface BookMarkListener {
        fun onFabClick()
        fun onCitySelected(bookMark: Bookmark)
    }

}