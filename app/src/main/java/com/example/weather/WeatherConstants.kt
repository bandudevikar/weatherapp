package com.example.weather

/**
 * Created by       : Bandu
 * Date             : 16/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
const val OPEN_WEATHER_API_KEY = "fae7190d7e6433ec3a45285ffcf55c86"