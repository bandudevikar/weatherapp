package com.example.weather

import android.app.Application
/**
 * Created by       : Bandu
 * Date             : 16/07/2021
 * Purpose/Usage    : Demo purpose to explain usage of Comments
 * Revisions        : xyz
 *			    Change – Add in add()
 *
 *                    2 - PQR
 *                    Change – Modify Substract()
 *
 * Additional Comments -
 */
class WeatherApplication : Application()